This library was designed primarily to serve as a FAT32 driver on bare-metal
systems and as such a heap is not available. This lib has been extended to
include some code that requires std to allow for debugging and to enable additional
features on non-bare-metal systems. **This is temporary during the development
process and this will become no_std again before the initial release.**

# Current focus
Rewrite current example to implement a file-store device backend. Then use my
FAT32 API to provide an API on top of it.

# Usage
Builder-style syntax is used to do various operations. The premise here is that
all operations start on the FileSystem object. No file operations can be done
on that object directly, instead file and folder operations can only be done on
`Directory` objects. Additionally, all operations operate exclusively in the
current directory. So there's no accessing `/foo/bar.txt` directory, the
directory structure must be traversed. Part of the reasoning for this is because
the structure of FAT makes all accesses folder-centric.

To access the root directory:
```
let root = fs.root(); // root is a `Directory`
```
To open the `hey.txt` file in the root directory:
```
let hey_file = fs.root().open_file("hey.txt"); // hey_file is a `File`
```
You can traverse the folder hierarchy by using `open_directory()`. To open the
file located at "/foo/bar/baz.txt":
```
fs.root().open_directory("foo").open_directory("bar").open_file("baz.txt");
```

`std::io::Read` and `std::io:Write` are not available in core, but equivalent `read()` and `write()` functions are available. Eventually this will be migrated to use `core_io`s traits in order to be compatible with other libraries in the nascent `no_std` Rust ecosystem.

# Requirements
Rust version 1.17 or newer.

# Tests
Various unit tests exist within the codebase for testing some of the simple
smaller FAT components. Additional integration tests using generated FAT32
images can be run on Linux. All of this can be tested fully-automatically with
the standard `cargo test` command.

For `cargo test` to execute successfully the following must also be installed on the system:
* fallocate
* mtools

## Modifying tests
Additional testing can be done by creating virtual drives and examining data on them. On Linux:

  1. Create a new virtual disk: `fallocate -l SIZE FILENAME`, where size is like 1G or 500M.
  2. Put a fat32 file system on it: `mkfs -t fat -F 32 FILENAME`
  3. Mount it using GNOME Disks or equivalent if you only need read-only access.
  4. For read/write access, mount it like `sudo mount -o loop examples/XXX.img /mnt/tmp` where /mnt/tmp is an empty folder and
     XXX is the name of the image file.

### Modifying images without root
Additionally it's possible to work with these disk images without requiring root
using `mtools` (available from most Linux distributions' repositories).

To list contents of a disk image:
```
mdir -i IMAGE_PATH
```
To copy a file into the disk image:
```
mcopy -i IMAGE_PATH FILEPATH DESTINATION_PATH
```
where DESTINATION_PATH is something like `::dir1/dir2/filename`.

To delete a file from the disk image:
```
mdir -i IMAGE_PATH FILEPATH
```
where FILEPATH is something like `::dir1/dir2/filename`.

To print the contents of a file:
```
mtype -i IMAGE_PATH FILEPATH
```

# Reference documentation
 * http://read.pudn.com/downloads77/ebook/294884/FAT32%20Spec%20(SDA%20Contribution).pdf
 * https://www.pjrc.com/tech/8051/ide/fat32.html
 * https://staff.washington.edu/dittrich/misc/fatgen103.pdf
 * http://elm-chan.org/fsw/ff/00index_e.html
